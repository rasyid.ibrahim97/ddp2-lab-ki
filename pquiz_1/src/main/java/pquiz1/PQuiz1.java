package pquiz1;

import pquiz1.enrollment.Student;
import pquiz1.reader.CsvReader;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * This class is the main program for programming quiz 1.
 *
 * @author TODO Write your name & NPM here
 * RASYID IBRAHIM S. = 1606863522
 */ 
public class PQuiz1 implements CsvReader {

    private static Path cwd = Paths.get("pquiz_1");

    public static void main(String[] args) throws IOException {
        Path dataFile = cwd.resolve("data/uni-x-students.csv");

        CsvReader reader = new ReaderV2(path);

        List<Student> students = reader.getStudents();
        System.out.println(String.format("There are %d unique students in the records",
                students.size()));

        List<Student> studentsWhoTakeKimiaDasar = reader.getStudentsEnrolledInCourse("Kimia Dasar");
        System.out.println(String.format("There are %d unique students that are taking Kimia Dasar",
                studentsWhoTakeKimiaDasar.size()));

        List<Student> studentsEnrolledIn2014 = reader.getStudentsByEnrollYear(2014);
        System.out.println(String.format("There are %d unique students that enrolled in 2014",
                studentsEnrolledIn2014.size()));

        // Reserved for demo with TA/lecturer

        // End of reserved section
    }
}
