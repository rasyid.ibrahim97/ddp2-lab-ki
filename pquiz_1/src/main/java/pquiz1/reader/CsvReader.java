package pquiz1.reader;

import pquiz1.enrollment.Student;

import java.util.List;

public interface CsvReader {
	
    List<Student> getStudents();
	
	List<Student> getStudentsEnrolledInCourse(String courseName);

    List<Student> getStudentsByEnrollYear(int year);
}
