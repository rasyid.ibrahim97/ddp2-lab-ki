package theater;

import java.util.ArrayList;
import ticket.Ticket;
import movie.Movie;

public class Theater{
    private String name;
    private int balance;
    private ArrayList<Ticket> ticketNumber;
    private Movie[] movie;

    public Theater(String name, int balance, ArrayList<Ticket> ticketNumber, Movie[] movie){
        this.name = name;
        this.balance = balance;
        this.ticketNumber = ticketNumber;
        this.movie = movie;
    }
    public void printInfo(){
        StringBuilder temp = new StringBuilder();
        for(int i=0;i<this.movie.length ;i++){
            if(i != this.movie.length -1){
                temp.append(String.format("%s, ", this.movie[i].getName()));
            }
            else{
                temp.append(this.movie[i].getName());
            }
        }
        System.out.println("------------------------------------------------------------------"+ 
        "\nCinema                          : " + this.name +
        "\nCash Balance                    : " + this.balance +
        "\nNumber of tickets available     : " + this.ticketNumber +
        "\nMovie list available            : " + temp +
        "\n------------------------------------------------------------------"
        );
    }
    public Movie getmovie(String title){
        for(int i = 0; i <movie.length;i++){
            if(this.movie[i].getTitle().equals(title)){
                return this.movie[i];
            }
        }
        return null;
    }

    public int getBalance(){
        return this.balance;
    }
    public String getName(){
        return this.name;
    }
    public boolean checkMovie(String title){
        for(int i=0; i< movie.length; i++){
            if(movie[i].getTitle().equals(title)){
                return True;
            }
        }
        return False;
    }
    public boolean checkTicket(Movie movie, String showTime, String type){
        boolean typeboolean = false;
        if(type.equals("3 Dimentions")){
            typeboolean = true;
        }
        for(int i = 0;i < this.movie.length;i++){
            if(this.ticketNumber.get(i).getMovies().getTitle().equals(movie.getTitle()) && this.ticketNumber.get(i).getShowtime().equals(showTime) &&((this.ticketNumber.get(i).isThreeD() && typeboolean)||!this.ticketNumber.get(i).isThreeD() && !typeboolean)){
                return true;
            }
        }
        return false;
    }
    public static void printTotalRevenue(Theater[] theaters){
        int revenue = 0;
        for(int i =0; i<theatres.length;i++){
            revenue += theaters[i].getBalance();
        }
        System.out.println("Koh Mas's total money: Rp." + revenue);
        System.out.println("------------------------------------------------------------------");
        for(int i=0;i<theater.length;i++){
            System.out.println("Cinema          :" + theaters[i].getName());
            System.out.println("Cash Balance    :" + theaters[i].getBalance());   
        }
        System.out.println("------------------------------------------------------------------");
    }
    public void removeTicket(Ticket ticket){
        for(int i=0;i<ticketNumber.size();i++){
            if(ticket.getMovies().getTitle().equals(ticketNumber.get(i).getMovies().getTitle()) &&
            ticket.getShowtime().equals(ticketNumber.get(i).getShowtime()) &&
            ((ticket.isThreeD() && ticketNumber.get(i).isThreeD()) || (!ticket.isThreeD() && ticketNumber.get(i).isThreeD()))){
                ticketNumber.remove(i);
            }
        }
    
    }
}
