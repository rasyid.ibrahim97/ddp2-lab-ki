package movie;

public class Movie{
    private String title;
    private String genre;
    private int duration;
    private String rating;
    private String type;
    
    public Movie(String title, String genre, int duration, String rating, String type){
        this.title = title;
        this.genre = genre;
        this.duration = duration;
        this.rating = rating;
        this.type = type;
    }
    public void pirntMovie(){
        System.out.println("------------------------------------------------------------------");
        System.out.println("Title       : " + this.title );
        System.out.println("Genre       : " + this.genre);
        System.out.println("Duration    : " + this.duration );
        System.out.println("Rating      : " + this.rating);
        System.out.println("Type        : " + this.type );
        System.out.println("------------------------------------------------------------------");
    }
    public String getTitle(){
        return this.title;
    }
    public int getRating(){
        if(this.rating.equals("General")){
            return 0;
        }
        else if(this.rating.equals("Teenagers")){
            return 13;
        }
        else{
            return 17;
        }
    }
}
