package ticket;
import movie.Movie;

public class Ticket{
    private Movie movies;
    private String showtime;
    private boolean threeD;
    private int price = 60000;

    public Ticket(Movie[] movies, String showtime, boolean threeD){
        this.movies = movies;
        this.showtime = showtime;
        this.threeD = threeD;

        if(showtime.equals("Saturday")|| showtime.equals("Sunday")){
            this.price += 40000;
        }
        if(this.threeD){
            this.price = this.price + (this.price/100*20);
        }
    }
    public void printTicket(){
        String temp = "Ordinary";
        if(this.threeD){
            temp = "3 Dimentions";
        }
        System.out.println("------------------------------------------------------------------");
        System.out.println("Movies      : "+ this.movies.getTitle());
        System.out.println("Showtimes   : "+ this.showtime);
        System.out.println("Type        : "+ temp);
        System.out.println("------------------------------------------------------------------");
    }
    public String getMovies(){
        return this.movies;
    }
    public String getShowtime(){
        return this.showtime;
    }
    public boolean isThreeD(){
        return this.threeD;
    }
    public int getPrice(){
        return this.price;
    }
}
