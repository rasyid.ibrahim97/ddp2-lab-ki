public class Manusia {
    private String nama;
    private int umur;
    private int uang = 50000;
    private double kebahagiaan = 50.0;

 public Manusia(String nama,int umur,int uang,double kebahagiaan){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = kebahagiaan;
        this.adjustkebahagiaan();
    }
    public String getnama(){
        return this.nama;
    }
    public int getumur(){
        return this.umur;
    }
    public int getuang(){
        return this.uang;
    }
    public double getkebahagiaan(){
        return this.kebahagiaan;
    }

    public void setnama(String nama){
        this.nama = nama;
    }
    public void setumur(int umur){
        this.umur = umur;
    }
    public void setuang(int uang){
        this.uang = uang;
    }
    public void setkebahagiaan(double kebahagiaan){
        this.kebahagiaan = kebahagiaan;
    }

    
    public void beriUang(Manusia guy){
        int amount = 0;
        for(int i = 0; i < guy.getnama().length(); i++){
            amount += (int)guy.getnama().charAt(i);
        }
        amount *= 100; 

         if (this.getuang() >= amount){
            this.setuang(this.getuang() - amount);
            guy.setuang(guy.getuang() + amount);
            this.setkebahagiaan(this.getkebahagiaan() + amount/6000.0);
            this.adjustkebahagiaan();
            guy.setkebahagiaan(guy.getkebahagiaan() + amount/6000.0);
            guy.adjustkebahagiaan();

            System.out.println(this.getnama() + " memberi uang sebanyak " + amount + " kepada " + guy.getnama() + ", mereka berdua senang :D");
        }else
        {
            System.out.println(this.getnama() + " ingin memberi uang kepada " + guy.getnama() + " namun tidak memiliki cukup uang :'(");
        }
    }
    public void beriUang(Manusia guy,int amount){
        if (this.getuang() >= amount){
            this.setuang(this.getuang() - amount); 
            guy.setuang(guy.getuang() + amount);

            this.setkebahagiaan(this.getkebahagiaan() + amount/6000.0);
            this.adjustkebahagiaan();
            guy.setkebahagiaan(guy.getkebahagiaan() + amount/6000.0);
            guy.adjustkebahagiaan();

            System.out.println(this.getnama() + " memberi uang sebanyak " + amount + " kepada " + guy.getnama() + ", mereka berdua senang :D");
        }else{
            System.out.println(this.getnama() + " ingin memberi uang kepada " + guy.getnama() + " namun tidak memiliki cukup uang :'(");
        }
    }
    public void bekerja(int durasi, int bebankerja){
        int bebanKerjaTotal = durasi * bebankerja;
        int pendapatan;

        if (bebanKerjaTotal <= this.getkebahagiaan()){
            this.setkebahagiaan(this.getkebahagiaan() - bebanKerjaTotal); 
            this.adjustkebahagiaan();
            pendapatan = bebanKerjaTotal * 10000;
            System.out.println(this.getnama() + " bekerja full time, total pendapatan : " + pendapatan);
        }

        else{
            int DurasiBaru = (int)(this.getkebahagiaan() / bebankerja); 
            bebanKerjaTotal = DurasiBaru * bebankerja;
            pendapatan = bebanKerjaTotal * 10000;
            this.setkebahagiaan(this.getkebahagiaan() - bebanKerjaTotal);
            this.adjustkebahagiaan();
            System.out.println(this.getnama() + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
        }

        this.setuang(this.getuang() + pendapatan); 
    }
    public void rekreasi(String namaTempat){
    int biaya = namaTempat.length() * 10000;

        if (this.getuang() >= biaya){
            this.setkebahagiaan(this.getkebahagiaan() + namaTempat.length());
            this.adjustkebahagiaan();
            this.setuang(this.getuang() - biaya);
            System.out.println(this.getnama() + " berekreasi di " + namaTempat + ", " + this.getnama() + " senang :)");
        }else{
            System.out.println(this.getnama() + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
        }
    }

    public void sakit(String namaPenyakit){
        this.setkebahagiaan(this.getkebahagiaan()-namaPenyakit.length());
        this.adjustkebahagiaan();
        System.out.println(this.getnama() + " Terkena Penyakit " + namaPenyakit);
    }
    public String toString(){
        return "Nama\t\t : " + this.getnama() + "\n" +
                "Umur \t\t :"+ this.getumur() + "\n" +
                "Uang \t\t :"+ this.getuang() + "\n" +
                "Kebahagiaan \t\t :" + this.getkebahagiaan();

        
    }
    private void adjustkebahagiaan(){
        if(this.getkebahagiaan() > 100.0){
            this.setkebahagiaan(100.0);
        }else if (this.getkebahagiaan() < 0.0){
            this.setkebahagiaan(0.0);
        }
    }
}
